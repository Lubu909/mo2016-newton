import java.util.Scanner;
import java.lang.Math;

public class Newton {
	
	private static int n;
	private double x[];
	private double y[];
	private double b[];
	
	Scanner reader = new Scanner(System.in);
	
	public Newton(){
		do {
			System.out.print("Podaj ilo��: ");
			n = reader.nextInt();
			if(n<0) System.out.println("Ilo�� musi by� wi�ksza lub r�wna 0!");
		} while(n<0);
		x = new double[n];
		y = new double[n];
		for(int i=0;i<n;i++){
			int flaga = 0;
			do{
				flaga = 0;
				System.out.print("Podaj x[" + i + "]: ");
				x[i] = reader.nextDouble();
				for(int j=0;j<i;j++){if(x[j]==x[i]) flaga=1;
				if(flaga==1) System.out.println("Ten x zosta� ju� podany, podaj inny");
				}
			}while(flaga==1);
			System.out.print("Podaj y[" + i + "]: ");
			y[i] = reader.nextDouble();
		}
	}
	
	public int getN(){
		return n;
	}
	
	void wypiszTablice(){
		System.out.printf("x  ||");
		for(int i=0; i<n; i++){
			System.out.printf(" %7.2f |",x[i]);
		}
		System.out.println();
		System.out.printf("y  ||");
		for(int i=0; i<n; i++){
			System.out.printf(" %7.2f |",y[i]);
		}
		System.out.println();
	}
	
	double b(int xmin, int xmax){		//xmin i xmax to miejsca(przedzial) w tablicy
		if(xmin==xmax) return y[xmin];
		else{
			return (b(xmin+1,xmax) - b(xmin,xmax-1))/(x[xmax] - x[xmin]);
		}
	}
	
	double[] wyznaczB(){ 
		b = new double[n];
		for(int i=n-1;i>=0;i--){
			b[i]=b(0,i);
		}
		return b;
	}
	
	static double[] liczNawiasy(double[] a, double[] b){
		int wielkosc = Math.max(a.length, b.length) + 1;
		double[] c = new double[wielkosc];
		for(int i=0;i<a.length;i++){
			for(int j=0;j<b.length;j++){
				c[i+j]+=a[i]*b[j];
			}
		}
		return c;
	}
	
	static void postacOgolna(double[] tab){
		for(int i=tab.length-1;i>=0;i--){
			if(tab[i]!=0){
				if(i==tab.length-1){
					if(tab[i]!=1) System.out.print(tab[i]);
					if(i>0){
						if(i>1) System.out.print("x^"+i);
						else System.out.print("x");
					}
				}
				else{
					if(tab[i]>0){ 
						System.out.print(" + ");
						if(tab[i]!=1 || i==0) System.out.print(tab[i]);
					}
					else{
						System.out.print(" - ");
						if(tab[i]!=-1 || i==0) System.out.print(-1*tab[i]);
					}
					if(i>0){
						if(i>1) System.out.print("x^"+i);
						else System.out.print("x");
					}
				}
			}
		}
		System.out.println();
	}
	
	double[] wyznaczWzor(){
		double polprodukt[][] = new double[n][];
		double wynik[] = new double[n];
		for(int i=0;i<n;i++){	//i to kolumna/i ze wzoru
			for(int j=0;j<i;j++){
				//wyliczanie nawiasow
				double[] pom = {-1*x[j],1};
				if(j==0) polprodukt[i] = pom;
				else polprodukt[i] = liczNawiasy(polprodukt[i],pom);
			}
			//przemnozenie tablicy z polproduktami przez b
			for(int j=0;j<=i;j++){
				if(i==0){
					polprodukt[i] = new double[1];
					polprodukt[i][j] = b[i];
				}
				else{
					polprodukt[i][j]*=b[i];
				}
			}
		}
		//zsumuj elementy tablicy polprodukt w tej samej kolumnie
		for (int currentRow = 0; currentRow < polprodukt.length; currentRow++) {
            for (int col = 0; col < polprodukt[currentRow].length; col++) {
                wynik[col] += polprodukt[currentRow][col];
            }
        }   
		return wynik;
	}
	
	double[] wyznaczPierwotna(double[] wynik){
		double[] pierwotna = new double[wynik.length+1];
		for(int i=0;i<wynik.length;i++) pierwotna[i+1] = wynik[i]/(i+1);
		return pierwotna;
	}
	
	double liczCalke(double[] pierwotna){
		double wynik = 0;
		double wynikA = 0;
		double wynikB = 0;
		int a,b;
		do{
			System.out.print("Podaj a: ");
			a = reader.nextInt();
			System.out.print("Podaj b: ");
			b = reader.nextInt();
			if(a>=b) System.out.println("a musi by� mniejsze od b");
		}while(a>=b);
		for(int i=0;i<pierwotna.length;i++){
			wynikA+=pierwotna[i]*Math.pow(a,i);
			wynikB+=pierwotna[i]*Math.pow(b,i);
		}
		wynik=wynikB-wynikA;
		return wynik;
	}
	
	public static void main(String[] args){
		Newton zestaw = new Newton();
		if(zestaw.getN()!=0){
			zestaw.wypiszTablice();
			double b[] = zestaw.wyznaczB();
			for(int i=0; i<n; i++) System.out.println("b[" + i + "]= " + b[i]);
			System.out.println();
			double[] wynik = zestaw.wyznaczWzor();
			System.out.print("Wz�r w postaci og�lnej: ");
			postacOgolna(wynik);
			double[] pierwotna = zestaw.wyznaczPierwotna(wynik);
			System.out.print("Pierwotna: ");
			postacOgolna(pierwotna);
			System.out.println("Ca�ka od a do b: " + zestaw.liczCalke(pierwotna));
		}
	}
}
